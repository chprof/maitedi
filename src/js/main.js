(function() {
	function parallax(elem, pos1, pos2, diff1, diff2) {
		$(window).mousemove(function(e) {
			var xpos=e.clientX;
			var ypos=e.clientY;
			var  xpos=xpos*2;
			ypos=ypos*2;
			$(elem).css(pos1, ((0-(ypos/diff1))+"px"));
			$(elem).css(pos2, ((0-(xpos/diff2))+"px"));
		});
	}
	// parallax('.front-section__bg-left', 'top', 'left', 100, 160);
	// parallax('.front-section__bg-right', 'top', 'right', 50, 100);
	// parallax('.front-section__bg-main', 'left', 'left', 40, 40);
	var wow = new WOW({
		live: true,
		mobile: false,
		boxClass: 'wow',
		animateClass: 'animated',
		offset: 0
	});
	wow.init();
	$('.video-wrap iframe').on('click', function() {
		$('.video-wrap').toggleClass('active');
	})

	if ( window.pageYOffset > 0 ) {
			$('.header').addClass('header_stick');
	} else {
		$('.header').removeClass('header_stick');
	}
	$(window).scroll(function() {
		var winT = $(this).scrollTop();
		if ( winT > 0 ) {
			$('.header').addClass('header_stick');
		} else {
			$('.header').removeClass('header_stick');
		}
	})
	$('body').scrollspy({target: ".scroll-spy"});


	$(".menu__list a").on('click', function(event) {
		var headerHeight = $('.header').outerHeight();
		if (this.hash !== "") {
			event.preventDefault();
			var hash = this.hash;
			$('html, body').animate({
				scrollTop: $(hash).offset().top - parseInt(headerHeight) - 10
			}, 800, function(){
				// window.location.hash = hash;
			});
		}
	});
	$('.menu-bar').click(function() {
		$('.menu').toggleClass('visible')
	})
	$('.menu-close').click(function() {
		$('.menu').removeClass('visible')
	})
}());