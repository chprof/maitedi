module.exports = function() {
    $.gulp.task("watch", function() {
        return new Promise((res, rej) => {
            $.gp.watch("./src/views/**/*.html", $.gulp.series("html"));
            $.gp.watch("./src/styles/**/*.scss", $.gulp.series("styles"));
            $.gp.watch("./src/js/**/*.js", $.gulp.series("scripts"));
            $.gp.watch("./src/css/**/*.css", $.gulp.series("cssVendor"));
            $.gp.watch("./src/fonts/**/*.{eot,woff,woff2,ttf,otf,ttf}", $.gulp.series("fonts"));
            $.gp.watch(["./src/images/**/*.{jpg,jpeg,png,gif,svg}", "!./src/images/icons/**/*", "!./src/images/favicons/**/*"], $.gulp.series("images"));
            $.gp.watch("./src/images/icons/svg/*.svg", $.gulp.series("iconfont"));
            $.gp.watch("./src/images/icons/sprite-components/**/*.png", $.gulp.series("sprite"));
            res();
        });
    });
};