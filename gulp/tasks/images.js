module.exports = function() {
    $.gulp.task("images", function() {
        return $.gulp.src(["./src/images/**/*.{jpg,jpeg,png,gif,svg}", "!./src/images/icons/**.*", "!./src/images/favicons/**/*"])
            .pipe($.gp.newer("./dest/images/"))
            .pipe($.gp.imagemin([
                $.imagemin.gifsicle({interlaced: true}),
                $.imagemin.jpegtran({progressive: true}),
                $.imageminJpegRecompress({loops: 1, quality: "low"}),
                $.imagemin.svgo(),
                $.imagemin.optipng({optimizationLevel: 5}),
                $.pngquant({quality: "65-70", speed: 5})
            ]))
            .pipe($.gulp.dest("./dest/images/"))
            .pipe($.debug({"title": "images"}))
            .on("end", $.bs.reload);
    });
};