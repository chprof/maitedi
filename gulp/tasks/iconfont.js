module.exports = function () {
    $.gulp.task("iconfont", function () {
        return $.gulp.src("./src/images/icons/svg/*.svg")
            .pipe($.iconfontcss({
                fontName: "iconsSvg",
                cssClass: 'iconSvg',
                targetPath: '../../src/styles/partials/_iconfontSvg.scss',
                fontPath: '../fonts/'
            }))
            .pipe($.iconfont({
                prependUnicode: false,
                fontName: "iconsSvg",
                formats: ["ttf", "eot", "svg", "woff", "woff2"],
                fontHeight: 1001, 
                normalize:true
            }))
            .pipe($.gulp.dest("./dest/fonts/"))
            .pipe($.debug({ "title": "iconfont" }));
    });
};