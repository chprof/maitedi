module.exports = function () {
    $.gulp.task('bowerCss', function() {
		return $.gulp.src($.mainFiles('**/*.css'))
		.pipe($.gulp.dest('./src/styles/libs'))
	  	.pipe($.debug({ "title": "bowerCss" }))
	  	.on("end", $.bs.reload);
	});
};