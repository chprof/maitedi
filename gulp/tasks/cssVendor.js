module.exports = function () {
    $.gulp.task("cssVendor", function () {
        return $.gulp.src("./src/styles/libs/*.css")
        	.pipe($.concat('vendor.css'))
        	.pipe($.mincss({ compatibility: "ie8" }))
        	.pipe($.gp.rename({ suffix: ".min" }))
            .pipe($.gulp.dest("./dest/styles/"))
            .pipe($.debug({ "title": "cssVendor" }))
            .on("end", $.bs.reload);
    });
};