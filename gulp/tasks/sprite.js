module.exports = function () {
    $.gulp.task("sprite", function () {
        var spriteData1 = $.gulp.src('./src/images/icons/sprite-components/sprite-components1/*.png')
            .pipe($.spritesmith({
                imgName: 'sprite1.png',
                cssName: 'sprite.css',
                padding: 20,
                algorithm: 'binary-tree',
                imgPath: '../images/sprites/sprite1.png'
            }));
        var spriteData2 = $.gulp.src('./src/images/icons/sprite-components/sprite-components2/*.png')
            .pipe($.spritesmith({
                imgName: 'sprite2.png',
                cssName: 'sprite.css',
                padding: 20,
                algorithm: 'binary-tree',
                imgPath: '../images/sprites/sprite2.png'
            }));
        var spriteData3 = $.gulp.src('./src/images/icons/sprite-components/sprite-components3/*.png')
            .pipe($.spritesmith({
                imgName: 'sprite3.png',
                cssName: 'sprite.css',
                padding: 20,
                algorithm: 'binary-tree',
                imgPath: '../images/sprites/sprite3.png'
            }));
        var spriteData4 = $.gulp.src('./src/images/icons/sprite-components/sprite-components4/*.png')
            .pipe($.spritesmith({
                imgName: 'sprite4.png',
                cssName: 'sprite.css',
                padding: 20,
                algorithm: 'binary-tree',
                imgPath: '../images/sprites/sprite4.png'
            }));
        var spriteData5 = $.gulp.src('./src/images/icons/sprite-components/sprite-components5/*.png')
            .pipe($.spritesmith({
                imgName: 'sprite5.png',
                cssName: 'sprite.css',
                padding: 20,
                algorithm: 'binary-tree',
                imgPath: '../images/sprites/sprite5.png'
            }));

        var imgStream = $.merge(spriteData1.img, spriteData2.img, spriteData3.img, spriteData4.img, spriteData5.img)
            .pipe($.buffer())
            .pipe($.imagemin())
            .pipe($.gulp.dest('./src/images/sprites/'));

        var cssStream = $.merge(spriteData1.css, spriteData2.css, spriteData3.css, spriteData4.css, spriteData5.css)
            .pipe($.concat('_sprite.scss'))
            .pipe($.gulp.dest('./src/styles/partials/'));

        return $.merge(imgStream, cssStream)
        .pipe($.debug({ "title": "sprite" }))
        .on("end", $.bs.reload);
    });
};
