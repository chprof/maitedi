module.exports = [
    "./gulp/tasks/sprite",
    "./gulp/tasks/html",
    "./gulp/tasks/styles",
    "./gulp/tasks/scripts",
    "./gulp/tasks/favicons",
    "./gulp/tasks/images",
    "./gulp/tasks/fonts",
    "./gulp/tasks/iconfont",
    "./gulp/tasks/watch",
    "./gulp/tasks/serve",
    "./gulp/tasks/bowerCss",
    "./gulp/tasks/bowerJs",
    "./gulp/tasks/cssVendor"
];